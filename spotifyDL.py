import spotipy
import spotipy.util as util
import sys
from spotipy.oauth2 import SpotifyClientCredentials
from apiclient.discovery import build
from youtube_dl import YoutubeDL
import urllib.request
from bs4 import BeautifulSoup
import requests
import os
import eyed3

# register an app on developer.spotify.com and fill in these credentials
CLIENT_ID = ''
CLIENT_SECRET = ''
REDIRECT_URI = ''

def show_tracks(tracks):
    songs = []
    for i, item in enumerate(tracks['items']):
        track = item['track']
        print ("   %d %32.32s %s" % (i, track['artists'][0]['name'],
            track['name']))
        song = track['artists'][0]['name'] + " - " + track['name']
        songs.append(song.encode('utf-8'))
    return songs

def getSpotifySongs():
    songs = []
    scope = 'user-library-read'

    if len(sys.argv) > 1:
        username = sys.argv[1]
    else:
        print ("Usage: %s username" % sys.argv[0],)
        sys.exit()

    token = util.prompt_for_user_token(username, scope, client_id=CLIENT_ID, client_secret=CLIENT_SECRET,redirect_uri=REDIRECT_URI)

    if token:
        sp = spotipy.Spotify(auth=token)

        playlists = sp.current_user_playlists()

        #select which playlists to download
        print(username, " playlists: ")
        print 
        for playlist in playlists['items']:
                print(playlist['name'])

        print
        while True:
            print("To show a playlist's tracks, type: showTracks(playlist_name). To download playlists' tracks, type: download(playlist1, playlist2, etc.). To display your playlists, type: showPlaylists")
            userinput = input()        
            if "showTracks" in userinput:
                name=userinput[userinput.find("(")+1:userinput.find(")")]
                for playlist in playlists['items']:
                    if playlist['name'] in name:
                        results = sp.user_playlist(username, playlist['id'],fields="tracks,next")

                        tracks = results['tracks']
                        show_tracks(tracks)
                continue
                    

            if "download" in userinput:
                tracks=userinput[userinput.find("(")+1:userinput.find(")")]
                tracklist = tracks.split(", ")
                break

            if "showPlaylists" in userinput:
                print
                for playlist in playlists['items']:
                    print(playlist['name'])
                print
                continue

            print    
            print("playlist name not recognised or incorrect input")
            print


        while playlists:

            for playlist in playlists['items']:
                if playlist['name'] not in tracklist:
                    continue
                print
                print (playlist['name'])
                print ('  total tracks', playlist['tracks']['total'])
                results = sp.user_playlist(username, playlist['id'],
                    fields="tracks,next")
                tracks = results['tracks']
            
                songlist = show_tracks(tracks)
                songs.append(songlist)
                
                while tracks['next']:
                    tracks = sp.next(tracks)
                 
            if playlists['next']:
                playlists = sp.next(playlists)
            else:
                playlists = None
    songsies = [item for sublist in songs for item in sublist]
    return songsies

def searchYT(songlist):
    songsies = []
    for song in songlist:
        textToSearch = song
        query = urllib.parse.quote(textToSearch)
        url = "https://www.youtube.com/results?search_query=" + query
        response = urllib.request.urlopen(url)
        html = response.read()
        soup = BeautifulSoup(html, 'html.parser')
        a = str(soup.contents)
        searchString = 'videoId'
        i = a.find(searchString)
        sub = a[i:i+30]
        songsies.append("https://www.youtube.com/watch?v="+sub[len(searchString)+3:sub.find('","')])
    return songsies
    

def downloadSongs(song_list):
    print(song_list)
    for song in song_list:
        ydl_opts = {
    'format': 'bestaudio/best',
    'postprocessors': [{
        'key': 'FFmpegExtractAudio',
        'preferredcodec': 'mp3',
        'preferredquality': '192'
    }]}
        with YoutubeDL(ydl_opts) as ydl:
            try:
                ydl.download([song])
            except:
                pass
    rename()

def rename():
    for filename in os.listdir("."):
        # split the file name into 3 parts
        if '-' in filename:
            split_val = filename.split('-')
            print(split_val)
            
            # concatenate the new name
            # my extension is .mp3 so I append that
            new_name = "%s-%s.mp3" % (split_val[0], split_val[1])
            print(new_name)
            
            # rename the file
            os.rename(filename, new_name)

# WIP
def addTags(songs):
    for filename in os.listdir("."):
        if 'mp3' in filename:
            audiofile = eyed3.load(filename)
            audiofile.tag.artist = ""
            audiofile.tag.title = "The Edge"
            audiofile.tag.save()

def downloadYTSongs():
    # example list of youtube song URLs
    songList = ['https://www.youtube.com/watch?v=Z8B2nBM0jFg', 'https://www.youtube.com/watch?v=939w8RwaLSY']
    downloadSongs(songList)
    rename()

def downloadSpotifySongs():
    songs = getSpotifySongs()
    songList = searchYT(songs)
    downloadSongs(songList)
    # addTags(songs)


def main():
    #downloadYTSongs()      # download from youtube
    downloadSpotifySongs()  # download from spotify


main()





